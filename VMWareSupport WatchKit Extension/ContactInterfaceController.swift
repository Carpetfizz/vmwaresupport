//
//  ContactInterfaceController.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/2/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import WatchKit
import Foundation
import VMWareSupportKit


class ContactInterfaceController: WKInterfaceController {
    
    @IBOutlet weak var lbl_name: WKInterfaceLabel!
    @IBOutlet weak var lbl_title: WKInterfaceLabel!
    @IBOutlet weak var tbl_phone: WKInterfaceTable!
    @IBOutlet weak var tbl_email: WKInterfaceTable!

    let emailActivityType = "com.vmware.vmwaresupport.email"
    let phoneActivityType = "com.vmware.vmwaresupport.phone"
    var contact: Contact?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        if let contact = context as? Contact {
            self.contact = contact
            self.lbl_name.setText(contact.contactName)
            self.lbl_title.setText(contact.title)
            
            self.loadPhones(contact.phones.allObjects as! [Phone])
            self.loadEmails(contact.emails.allObjects as! [Email])
        }
    }
    
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        if(table == self.tbl_phone){
            let selectedPhone = (self.contact!.phones.allObjects as! [Phone])[rowIndex]
            self.updateUserActivity(phoneActivityType, userInfo: ["contactPhone": selectedPhone.number], webpageURL: nil)
        }else{
            let selectedEmail = (self.contact!.emails.allObjects as! [Email])[rowIndex]
            self.updateUserActivity(emailActivityType, userInfo: ["contactEmail": selectedEmail.address], webpageURL: nil)
        }
    }
    
    func loadPhones(phones: [Phone]) {
        tbl_phone.setNumberOfRows(phones.count, withRowType: "PhoneRow")
        for (pIndex, phone) in enumerate(phones){
            
            if let row = tbl_phone.rowControllerAtIndex(pIndex) as? PhoneRow {
                row.lbl_type.setText(phone.type.lowercaseString)
                row.lbl_number.setText(phone.number)
            }
        }
    }
    
    func loadEmails(emails: [Email]) {
        tbl_email.setNumberOfRows(emails.count, withRowType: "EmailRow")
        for (eIndex, email) in enumerate(emails){
            if let row = tbl_email.rowControllerAtIndex(eIndex) as? EmailRow {
                row.lbl_type.setText(email.type)
                row.lbl_email.setText(email.address)
            }
        }
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
