//
//  ContactRow.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/1/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit
import WatchKit

class ContactRow: NSObject {
    
    @IBOutlet weak var lbl_name: WKInterfaceLabel!
    @IBOutlet weak var lbl_title: WKInterfaceLabel!
    
    @IBOutlet weak var gr_type: WKInterfaceGroup!
    @IBOutlet weak var lbl_type: WKInterfaceLabel!
    
}
