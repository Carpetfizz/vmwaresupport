//
//  PhoneRow.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/3/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import WatchKit

class PhoneRow: NSObject {
    @IBOutlet weak var lbl_type: WKInterfaceLabel!
    @IBOutlet weak var lbl_number: WKInterfaceLabel!
}
