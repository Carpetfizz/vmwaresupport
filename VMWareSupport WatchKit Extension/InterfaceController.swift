//
//  InterfaceController.swift
//  VMWareSupport WatchKit Extension
//
//  Created by Ajay Ramesh on 8/1/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import WatchKit
import Foundation
import VMWareSupportKit

class InterfaceController: WKInterfaceController {    
    
    @IBOutlet weak var tbl_contacts: WKInterfaceTable!
    @IBOutlet weak var lbl_warning: WKInterfaceLabel!
    var contacts: [Contact]?
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String,
        inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
            if segueIdentifier == "toContact" {
                if let contacts = self.contacts {
                    let contact = contacts[rowIndex]
                    return contact
                }
              }
            return nil
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.refreshData()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    func refreshData(){
        if let customer = CoreDataAccess.sharedInstance.getSavedCustomer() {
            self.tbl_contacts.setHidden(false)
            self.lbl_warning.setHidden(true)
            self.setTitle(customer.customerName)
            self.contacts = CoreDataAccess.sharedInstance.getSortedContacts(customer.contacts)
            self.loadContacts(self.contacts!)
        }else{
            self.tbl_contacts.setHidden(true)
            self.lbl_warning.setHidden(false)
        }
    }
    
    func loadContacts(contacts:[Contact]) {
        
        tbl_contacts.setNumberOfRows(contacts.count, withRowType: "ContactRow")
        
        for (index, contact) in enumerate(contacts) {
            if let row = tbl_contacts.rowControllerAtIndex(index) as? ContactRow {
                let contactType: ContactType = contact.type as! ContactType
                let typeLetter = String(contactType.name[advance(contactType.name.startIndex, 0)])
                
                row.gr_type.setBackgroundImage(UIImage(named: "ContactBackground"))
    
                row.lbl_type.setText(typeLetter)
                row.lbl_name.setText(contact.contactName)
                row.lbl_title.setText(contact.title)
            }
        }
        
    }

}
