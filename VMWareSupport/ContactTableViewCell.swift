//
//  ContactTableViewCell.swift
//  VMWare TAM
//
//  Created by Ajay Ramesh on 7/30/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit


class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_type: UIImageView!
    @IBOutlet weak var lbl_type: UILabel!
    
    var index: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}