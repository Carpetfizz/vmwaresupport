//
//  ContactInfoTableViewCell.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/4/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit

class ContactInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_type: UILabel!
    @IBOutlet weak var lbl_info: UILabel!
    @IBOutlet weak var img_info_type: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
