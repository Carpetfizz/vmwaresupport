//
//  ViewController.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/1/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit
import VMWareSupportKit

class ViewController: UIViewController, UITableViewDataSource, RequestManagerDelegate, AccessCodeDelegate {
    
    @IBOutlet weak var tbl_contacts: UITableView!
    @IBOutlet weak var navbar: UINavigationItem!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var rm = RequestManager()
    var contacts: [Contact]?
    let cellIdentifier = "ContactCell"
    var activityIndicator: UIActivityIndicatorView?
    var isRefreshing: Bool = false
    
    //MARK: - UI Callbacks
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityIndicator!.hidesWhenStopped = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        rm.delegate = self
        if let oldCustomer = CoreDataAccess.sharedInstance.getSavedCustomer() {
            self.navigationItem.title = oldCustomer.customerName
            self.contacts = CoreDataAccess.sharedInstance.getSortedContacts(oldCustomer.contacts)
            tbl_contacts.reloadData()
        }else{
            self.checkAccessCode()
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toContact" {
            if let contactVC: ContactViewController = segue.destinationViewController as? ContactViewController {
                if let contactIndex = tbl_contacts.indexPathForSelectedRow()?.row {
                    contactVC.contact = self.contacts![contactIndex]
                }
            }
        }else if segue.identifier == "toAccessCode" {
            if let accessVC: AccessCodeViewController = segue.destinationViewController as? AccessCodeViewController {
                accessVC.delegate = self;
            }
        }
    }
    
    //MARK: - Table View Delegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let contacts = self.contacts {
            return contacts.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = cellIdentifier
        
        var cell: ContactTableViewCell = tableView.dequeueReusableCellWithIdentifier(identifier) as! ContactTableViewCell
        
        let contact = self.contacts![indexPath.row] as Contact
        
        let contactType: ContactType = contact.type as! ContactType
        //let typeLetter = String(contactType.name[advance(contactType.name.startIndex, 0)])
    
        cell.img_type.image = UIImage(named: "ContactBackground")
        
        cell.lbl_type.text = contactType.name
        
        cell.lbl_name.text = contact.contactName
        cell.lbl_title.text = contact.title

        cell.index = indexPath.row
        
        return cell
    }
    
    //MARK: - Request Manger Delegate
    
    func didSaveCustomer() {
        if let currentCustomer = CoreDataAccess.sharedInstance.getSavedCustomer(){
            activityIndicator!.stopAnimating()
            self.navigationItem.titleView = nil
            self.navigationItem.title = currentCustomer.customerName
            self.contacts = CoreDataAccess.sharedInstance.getSortedContacts(currentCustomer.contacts)
            self.tbl_contacts.reloadData()
            self.isRefreshing = false
        }
    }
    
    //MARK: - Access Code Delegate 
    
    func didGetAccessCode() {
        self.checkAccessCode()
    }
    
    func didError(title: String, message: String) {
        self.showErrorAlert(title, message: message)
    }
    
    @IBAction func didTouchReset(sender: AnyObject) {
        self.showAccess()
    }
    
    @IBAction func didTouchRefresh(sender: AnyObject) {
        if(!self.isRefreshing){
            self.checkAccessCode()
        }
    }
    
    //MARK: - Helper Functions
    
    func showAccess() {
        self.performSegueWithIdentifier("toAccessCode", sender: self)
    }
    
    func showErrorAlert(title: String, message: String) {
        var errorAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        errorAlert.addAction(UIAlertAction(title: "Try again", style: .Default, handler: { (alertAction) in
            errorAlert.dismissViewControllerAnimated(true, completion: nil)
            self.showAccess()
        }))
        self.presentViewController(errorAlert, animated: true, completion: nil)
    }
    
    func checkAccessCode() {
        
        if let accessCode: NSNumber = self.defaults.objectForKey("accessCode") as? NSNumber {
            self.navigationItem.titleView = activityIndicator!
            activityIndicator!.startAnimating()
            self.isRefreshing = true
            rm.initiateCustomerRequestWith(accessCode)
        }else{
           self.showAccess()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
