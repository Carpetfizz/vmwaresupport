//
//  ContactViewController.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/4/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit
import VMWareSupportKit

class ContactViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var tbl_info: UITableView!
    
    var contact: Contact?
    var sectionTitles: [String] = ["Phones", "Emails"]
    var contactInfo: [String: [AnyObject]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let contact = self.contact {
            self.lbl_name.text = contact.contactName
            self.lbl_title.text = contact.title
            
            let contactPhones = contact.phones.allObjects as! [Phone]
            let contactEmails = contact.emails.allObjects as! [Email]
            
            var phones: [Phone] = []
            var emails: [Email] = []
            
            for phone: Phone in contactPhones {
                phones.append(phone)
            }
            
            for email: Email in contactEmails {
                emails.append(email)
            }
            
            self.contactInfo["Emails"] = emails
            self.contactInfo["Phones"] = phones

        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.sectionTitles.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactInfo[self.sectionTitles[section]]!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "ContactInfoCell"
        var infoCell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! ContactInfoTableViewCell
        
        let sectionTitle = self.sectionTitles[indexPath.section]
        
        
        let object: AnyObject = self.objectForIndexPath(indexPath)
        
        
        if let phone = object as? Phone {
            infoCell.lbl_info.text = phone.number
            infoCell.lbl_type.text = phone.type
            infoCell.img_info_type.image = UIImage(named: "Phone")
        }
        
        if let email = object as? Email {
            infoCell.lbl_info.text = email.address
            infoCell.lbl_type.text = email.type
            infoCell.img_info_type.image = UIImage(named: "Mail")
        }
        
        return infoCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let object: AnyObject = self.objectForIndexPath(indexPath)
        
        if let phone = object as? Phone {
            let phoneURL = NSURL(string: "tel://\(phone.number)")
            UIApplication.sharedApplication().openURL(phoneURL!)
        }
        
        if let email = object as? Email {
            let emailURL = NSURL(string: "mailto:\(email.address)")
            UIApplication.sharedApplication().openURL(emailURL!)
        }
    }
    
    
    //MARK: - Helper Functions
    
    func objectForIndexPath(path: NSIndexPath) -> AnyObject {
        
        var object: AnyObject?
        
        let sectionTitle = self.sectionTitles[path.section]
        
        if sectionTitle == "Phones" {
            if let pa = self.contactInfo[sectionTitle]{
                object =  pa[path.row] as! Phone
            }
        }else {
            if let ea = self.contactInfo[sectionTitle]{
                object = ea[path.row] as! Email
            }
        }
        
        return object!
    }

}
