//
//  AccessCodeViewController.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/17/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit

protocol AccessCodeDelegate {
    func didGetAccessCode()
}

class AccessCodeViewController: UIViewController {

    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var tf_accessCode: UITextField!
    let defaults = NSUserDefaults.standardUserDefaults()
    var delegate: AccessCodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .Cancel,
            target: self, action: Selector("didPressCancelAccess:"))
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: self, action: Selector("didPressDone:"))
        keyboardToolbar.items = [cancelBarButton, flexBarButton, doneBarButton]
        tf_accessCode.inputAccessoryView = keyboardToolbar

        if let accessCode: NSNumber = self.defaults.objectForKey("accessCode") as? NSNumber {
            tf_accessCode.text = accessCode.stringValue
            btn_cancel.hidden = false
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressCancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didPressCancelAccess(sender: AnyObject) {
        self.tf_accessCode.resignFirstResponder()
    }
    
    func didPressDone(sender: AnyObject){
        if(count(tf_accessCode.text) > 0 ){
            let accessCode: String = tf_accessCode.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            self.defaults.setObject(NSNumber(integer: accessCode.toInt()!), forKey: "accessCode")
            self.tf_accessCode.resignFirstResponder()
            if let delegate = self.delegate {
                delegate.didGetAccessCode()
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

}
