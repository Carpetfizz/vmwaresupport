//
//  AppDelegate.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/1/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import UIKit
import VMWareSupportKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let emailActivity = "com.vmware.vmwaresupport.email"
    let phoneActivity = "com.vmware.vmwaresupport.phone"


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().barTintColor = UIColor(red: 0/255.0, green: 149/255.0, blue: 211/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
                
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataAccess.sharedInstance.saveContext()
    }
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]!) -> Void) -> Bool {
        if (userActivity.activityType == emailActivity) {
            if let userInfo = userActivity.userInfo as? [String : AnyObject] {
                let contactEmail: String = userInfo["contactEmail"]! as! String
                let emailURL = NSURL(string: "mailto:\(contactEmail)")
                UIApplication.sharedApplication().openURL(emailURL!)
            }
        }else if(userActivity.activityType == phoneActivity) {
            if let userInfo = userActivity.userInfo as? [String : AnyObject] {
                let contactPhone: String = userInfo["contactPhone"]! as! String
                let phoneURL = NSURL(string: "tel://\(contactPhone)")
                UIApplication.sharedApplication().openURL(phoneURL!)
            }
        }
        return false
    }
    
    
}

