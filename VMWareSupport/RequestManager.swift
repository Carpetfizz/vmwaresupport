//
//  RequestManager.swift
//  VMWare TAM
//
//  Created by Ajay Ramesh on 7/29/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import CoreData
import VMWareSupportKit

protocol RequestManagerDelegate {
    func didSaveCustomer()
    func didError(title: String, message: String)
}

struct RequestManager {
    
    var delegate: RequestManagerDelegate?
    let metadataString = "https://api-dev.vmware.com/dev12/v1/metadata"

    func initiateCustomerRequestWith(accessCode: NSNumber) {
        
        let mashreyUser: String = "qdqe64nd392y9bgbqj4fudsj"
        let mashreyPass: String = "uc9rB2cpEmsr4MJcEncTECDZ"
        let tokenString: String = "https://api-dev.vmware.com/dev/token"
        
        let tokenURL = NSURL(string: tokenString)
        let tokenRequest = NSURLRequest(URL: tokenURL!)
        let tokenRequestHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
        let tokenRequestParams = ["grant_type":"client_credentials"]
        
        request(.POST, tokenRequest, headers: tokenRequestHeaders, parameters: tokenRequestParams)
            .authenticate(user: mashreyUser, password: mashreyPass)
            .responseJSON { (Request, ResponseSerializer, data, error) in
                if let e = error {
                    println("API Token Error: \(e.userInfo)")
                    if let delegate = self.delegate {
                        delegate.didError("API Token Error", message: "Unable to retrieve API token, please contact administrator")
                    }
                }else if let data: AnyObject = data {
                    
                    let tokenInfo = JSON(data)
                    let accessToken = tokenInfo["access_token"].string!
                    self.fetchCustomerWith(accessToken, accessCode: accessCode)
                    
                }
            }
    }
    
    func fetchCustomerWith(accessToken: String, accessCode: NSNumber) {
        
        let metadataURL = NSURL(string: metadataString)
        let metadataRequest = NSURLRequest(URL: metadataURL!)
        let resourceRequestHeader: [String: String] =  ["Authorization":"Bearer "+accessToken]

        var customerJSON: JSON?
        
        
        request(.GET, metadataRequest, headers: resourceRequestHeader)
            .responseJSON { (requeste, response, data, error) in
                if let e = error {
                    println("Metadata response error: \(e.userInfo)")
                    if let delegate = self.delegate {
                        delegate.didError("Metadata Error", message: "Unable to fetch metadata, please make sure the metadata URL is correct")
                    }
                }else if let data: AnyObject = data {
                    
                    let metadata = JSON(data)
                    let customerString = metadata["url"].string!+accessCode.stringValue
                    let customerRequest = NSURLRequest(URL: NSURL(string: customerString)!)
                    
                    request(.GET,customerRequest, headers: resourceRequestHeader)
                        .responseJSON { (request, response, data, error) in
                            if let e = error {
                                if response?.statusCode == 404 {
                                    println("Customer response error: \(e.userInfo)")
                                    if let delegate = self.delegate {
                                        delegate.didError("Invalid Access Code", message: "Customer not found, please enter the correct access code")
                                    }
                                }
                                else {
                                    if let delegate = self.delegate {
                                        delegate.didError("Customer Error", message: "Unable to fetch customers, please contact administrator")
                                    }
                                }
                            }else if let customer: AnyObject = data {
                                let customerJSON = JSON(customer)["customer"]
                                if let oldCustomer = CoreDataAccess.sharedInstance.getSavedCustomer() {
                                    CoreDataAccess.sharedInstance.deleteCustomer(oldCustomer)
                                }
                                self.saveCustomer(customerJSON)
                                if let delegate = self.delegate {
                                    delegate.didSaveCustomer()
                                }
                            }
                    }
                }
        }
    }
    
    func saveCustomer(customerJSON: JSON) {
        let managedContext = CoreDataAccess.sharedInstance.managedObjectContext!
        
        let customerEntity = NSEntityDescription.entityForName("Customer", inManagedObjectContext: managedContext)
        let customer = NSManagedObject(entity: customerEntity!, insertIntoManagedObjectContext: managedContext)
        
        var error: NSError?
        
        customer.setValue(customerJSON["customerName"].string!, forKey: "customerName")
        customer.setValue(customerJSON["accessCode"].int!, forKey: "accessCode")
        
        let contactList = customerJSON["contacts"]
        
        for (contactIndex: String, contactJSON: JSON) in contactList {
            let contactEntity = NSEntityDescription.entityForName("Contact", inManagedObjectContext: managedContext)
            let contact = NSManagedObject(entity: contactEntity!, insertIntoManagedObjectContext: managedContext)
            contact.setValue(contactJSON["name"].string ?? "Unavailable", forKey: "contactName")
            contact.setValue(contactJSON["title"].string ?? "Unavailable ", forKey: "title")
            
            let phoneList = contactJSON["phoneNumbers"]
            let emailList = contactJSON["emailAddresses"]

            
            for (phoneIndex: String, phoneNumber: JSON) in phoneList {
                let phoneEntity = NSEntityDescription.entityForName("Phone", inManagedObjectContext: managedContext)
                let phone = NSManagedObject(entity: phoneEntity!, insertIntoManagedObjectContext: managedContext)
                phone.setValue(phoneNumber["number"].string ?? "Unavailable", forKey: "number")
                phone.setValue(phoneNumber["type"].string ?? "Unavailable" , forKey: "type")
                phone.setValue(contact, forKey: "contact")
            }
            
            for (emailIndex: String, emailAddress: JSON) in emailList {
                let emailEntity = NSEntityDescription.entityForName("Email", inManagedObjectContext: managedContext)
                let email = NSManagedObject(entity: emailEntity!, insertIntoManagedObjectContext: managedContext)
                email.setValue(emailAddress["address"].string ?? "Unavailable" , forKey: "address")
                email.setValue(emailAddress["type"].string ?? "Unavailable", forKey: "type")
                email.setValue(contact, forKey: "contact")
            }
            
            let typeEntity = NSEntityDescription.entityForName("Type", inManagedObjectContext: managedContext)
            let type = NSManagedObject(entity: typeEntity!, insertIntoManagedObjectContext: managedContext)
            
            type.setValue(contactJSON["type"]["name"].string ?? "Unavailable", forKey: "name")
            type.setValue(contact, forKey: "contact")
            type.setPrimitiveValue(contactJSON["type"]["level"].int, forKey: "level")
            /* Currently setting the contact severity as the level of their type */
            contact.setValue(contactJSON["type"]["level"].int, forKey: "severity")
            contact.setValue(customer, forKey: "customer")
        }
        
        managedContext.save(&error)
    }
    
}