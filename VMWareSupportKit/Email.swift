//
//  Email.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/3/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import CoreData

public class Email: NSManagedObject {

    @NSManaged public var address: String
    @NSManaged public var type: String
    @NSManaged public var contact: Contact

}
