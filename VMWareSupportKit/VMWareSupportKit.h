//
//  VMWareSupportKit.h
//  VMWareSupportKit
//
//  Created by Ajay Ramesh on 8/1/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VMWareSupportKit.
FOUNDATION_EXPORT double VMWareSupportKitVersionNumber;

//! Project version string for VMWareSupportKit.
FOUNDATION_EXPORT const unsigned char VMWareSupportKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VMWareSupportKit/PublicHeader.h>


