//
//  Contact.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/3/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import CoreData

public class Contact: NSManagedObject {

    @NSManaged public var contactName: String
    @NSManaged public var severity: NSNumber
    @NSManaged public var title: String
    @NSManaged public var customer: Customer
    @NSManaged public var emails: NSSet
    @NSManaged public var phones: NSSet
    @NSManaged public var type: NSManagedObject

}
