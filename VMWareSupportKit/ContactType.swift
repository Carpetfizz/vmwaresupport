//
//  ContactType.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/3/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import CoreData

public class ContactType: NSManagedObject {

    @NSManaged public var name: String
    @NSManaged public var level: NSNumber
    @NSManaged public var contact: Contact

}
