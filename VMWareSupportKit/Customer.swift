//
//  Customer.swift
//  VMWareSupport
//
//  Created by Ajay Ramesh on 8/3/15.
//  Copyright (c) 2015 VMWare. All rights reserved.
//

import Foundation
import CoreData

public class Customer: NSManagedObject {

    @NSManaged public var accessCode: NSNumber
    @NSManaged public var customerName: String
    @NSManaged public var imageURL: String
    @NSManaged public var contacts: NSSet

}
